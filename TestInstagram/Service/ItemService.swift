//
//  ItemService.swift
//  TestInstagram
//
//  Created by Dmitry Lavryk on 12.06.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

class ItemService: NSObject {
    
    static let shared = ItemService()
    
    private override init() {
        super.init()
    }
    
    var items = [ItemModel]()
    
    func getItem(completion: @escaping (Error?) -> ()) {
        
        if let url = Bundle.main.url(forResource: "Contents", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let jsonData = try JSONDecoder().decode([ItemModel].self, from: data)
                items = jsonData
                completion(nil)
            } catch let error {
                completion(error)
            }
        }
    }
}
