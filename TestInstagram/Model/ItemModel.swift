//
//  ItemModel.swift
//  TestInstagram
//
//  Created by Dmitry Lavryk on 12.06.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import Foundation

class ItemModel: Codable {
    
    let imageUser: String?
    let nameUser: String?
    let locationUser: String?
    let userPhoto: [String]?
    let numberOfLikes: [String]?
    let comments: String?
    let date: Int?
}
