import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {
    
    private var mainVC: MainViewController = {
        let vc: MainViewController = MainViewController()
        vc.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "house"), tag: 0)
        return vc
    }()
    
    private var searchVC: UIViewController = {
                let vc: UIViewController = UIViewController()
        vc.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "magnifyingglass"), tag: 1)
        return vc
    }()
    
    private var addPhotoVC: UIViewController = {
                let vc: UIViewController = UIViewController()
        vc.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "plus.rectangle.fill"), tag: 1)
        return vc
    }()
    
    private var favoriteVC: UIViewController = {
                let vc: UIViewController = UIViewController()
        vc.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "heart"), tag: 1)
        return vc
    }()
    
    private var profileVC: UIViewController = {
                let vc: UIViewController = UIViewController()
        vc.tabBarItem = UITabBarItem(title: nil, image: UIImage(systemName: "person.crop.circle"), tag: 1)
        return vc
    }()
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return self.selectedViewController?.preferredStatusBarStyle ?? .default
    }
    
    private func setup() {
        
        UITabBar.appearance().tintColor = .black
        UITabBar.appearance().unselectedItemTintColor = .gray
        
        let viewControllers: [UIViewController] = [
            mainVC,
            searchVC,
            addPhotoVC,
            favoriteVC,
            profileVC
        ]
        
        self.viewControllers = viewControllers
    }
}

