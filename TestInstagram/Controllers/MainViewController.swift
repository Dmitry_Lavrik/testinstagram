//
//  ViewController.swift
//  TestInstagram
//
//  Created by Dmitry Lavryk on 11.06.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit
import SnapKit

class MainViewController: UIViewController {
    
    let tableView = UITableView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupTableView()
        
        setupTitleFont()
        
        setupBarButtons()
        
        getItem()
    }
    
    private func getItem() {
        
        ItemService.shared.getItem { [weak self] (error) in
            
            if let err = error {
                
                print(err)
            } else {
                
                self?.tableView.reloadData()
            }
        }
    }
    
    private func setupTableView() {
        
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        
        tableView.register(InstagramTableViewCell.self, forCellReuseIdentifier: String(describing: InstagramTableViewCell.self))
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func setupTitleFont() {
        
        tabBarController?.title = "Instagram"
        
        if let myFont = UIFont(name: "Billabong", size: 32) {
            
            let attributes = [NSAttributedString.Key.font: myFont]
            UINavigationBar.appearance().titleTextAttributes = attributes
        }
    }
    
    private func setupBarButtons() {
        
        let leftBarButton = UIBarButtonItem(image: UIImage(systemName: "camera"), style: .plain, target: self, action: nil)
        leftBarButton.tintColor = .black
        tabBarController?.navigationItem.setLeftBarButton(leftBarButton, animated: true)
        
        let rightBarButton = UIBarButtonItem(image: UIImage(systemName: "paperplane"), style: .plain, target: self, action: nil)
        rightBarButton.tintColor = .black
        tabBarController?.navigationItem.setRightBarButton(rightBarButton, animated: true)
    }
}

extension MainViewController: UITableViewDelegate {
    
}

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ItemService.shared.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCell(withIdentifier: String(describing: InstagramTableViewCell.self), for: indexPath) as! InstagramTableViewCell
        
        cell.selectionStyle = .none
        
        cell.model = ItemService.shared.items[indexPath.row]
        
        return cell
    }
}
