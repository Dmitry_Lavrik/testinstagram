//
//  NSMutableAttributedString+Help.swift
//  TestInstagram
//
//  Created by Dmitry Lavryk on 12.06.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor) {
        
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        
        self.addAttributes([.foregroundColor: color, .font: UIFont.systemFont(ofSize: 14, weight: UIFont.Weight.semibold)], range: range)
    }
}
