import Foundation
import Kingfisher

extension UIImageView {
    
    func sm_setImageWith(url aUrl: URL?) {
        
        self.sm_setImageWith(url: aUrl, useActivity: true)
    }
    
    func sm_setImageWith(url aUrl: URL?, useActivity aIsUseActivity: Bool) {
        
        if aIsUseActivity {
            self.kf.indicatorType = .activity
            self.kf.indicatorType = .activity
        } else {
            
            self.kf.indicatorType = .none
        }
        
        self.kf.setImage(with: aUrl)
    }
    
    func animateWithFade(image aImage: UIImage, diration aDuration: TimeInterval = 1) {
        
        UIView.transition(with: self, duration: aDuration, options: .transitionCrossDissolve, animations: {
                            
            self.image = aImage
        }, completion: nil)
        
    }
}
