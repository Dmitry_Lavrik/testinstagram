//
//  InstagramTableViewCell.swift
//  TestInstagram
//
//  Created by Dmitry Lavryk on 11.06.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import UIKit
import SnapKit
import ImageSlideshow

class InstagramTableViewCell: UITableViewCell {
    
    let imageUserIV = UIImageView()
    private let nameUserBt = UIButton()
    private let locationUserBt = UIButton()
    private let informationsBt = UIButton()
    private let photoIS = ImageSlideshow()
    private let pageIndicatorPC = UIPageControl()
    private let likeBt = UIButton()
    private let commentBt = UIButton()
    private let sendBt = UIButton()
    private let favoriteBt = UIButton()
    private let numberOfLikesLb = UILabel()
    private let commentsLb = UILabel()
    private let dateLb = UILabel()
    
    var model: ItemModel? {
        
        didSet {
            
            guard let model = model else { return }
            
            imageUserIV.sm_setImageWith(url: URL(string: model.imageUser ?? ""))
            
            nameUserBt.setTitle(model.nameUser, for: .normal)
            
            locationUserBt.setTitle(model.locationUser, for: .normal)
            
            if let userPhotos = model.userPhoto?.compactMap({ KingfisherSource(url: URL(string: $0)!)}) {
                
                photoIS.setImageInputs(userPhotos)
            }
            
            setupDataOfNumberOfLikes(model)
            
            setupDataOfCommentsLb(model)
            
            setupDataOfDate(model)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        setupImageUser()
        setupInformation()
        setupNameUser()
        setupLocationUser()
        setupPhoto()
        setupPageIndicator()
        setupLikeButton()
        setupCommentButton()
        setupSendButton()
        setupFavoriteButton()
        setupNumberOfLikesLable()
        setupCommentsLable()
        setupDateLabel()
    }
    
    private func setupDataOfNumberOfLikes(_ model: ItemModel) {
        
        if let numberOfLikes = model.numberOfLikes?.compactMap({ $0}) {
            
            numberOfLikesLb.font = UIFont.systemFont(ofSize: 14)
            
            if numberOfLikes.count < 3 {
                
                var string = ""
                
                numberOfLikes.forEach({ (userName) in
                    string += " \(userName),"
                })
                string.removeLast()
                
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "Liked by\(string)")
                
                numberOfLikes.forEach { (string) in
                    
                    setColor(attributedString: attributedString, label: numberOfLikesLb, string: string)
                }
                
            } else {
                
                let LikedByString = "Liked by \(numberOfLikes[0]), \(numberOfLikes[1]), \(numberOfLikes[2]) and \(numberOfLikes.count - 3) others"
                
                let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: LikedByString)
                
                numberOfLikes.forEach { (string) in
                    
                    setColor(attributedString: attributedString, label: numberOfLikesLb, string: string)
                }
                
                setColor(attributedString: attributedString, label: numberOfLikesLb, string: "\(numberOfLikes.count - 3) others")
            }
        }
    }
    
    private func setupDataOfCommentsLb(_ model: ItemModel) {
        
        commentsLb.font = UIFont.systemFont(ofSize: 14)
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "\(model.nameUser!) \(model.comments!)")
        
        setColor(attributedString: attributedString, label: commentsLb, string: model.nameUser!)
        
        let commentsArray = model.comments!.components(separatedBy: " ").filter{($0.hasPrefix("@") || $0.hasPrefix("#") )}
        
        commentsArray.forEach { (comment) in
            
            setColor(attributedString: attributedString, label: commentsLb, string: comment, color: .blue)
        }
    }
    
    private func setupDataOfDate(_ model: ItemModel) {
        
        let timeInterval = Double(model.date!)
        
        let date = Date(timeIntervalSince1970: timeInterval)
        
        dateLb.text = date.timeAgoSinceDate()
    }
    
    private func setupImageUser() {
        
        contentView.addSubview(imageUserIV)
        
        imageUserIV.contentMode = .scaleToFill
        
        imageUserIV.clipsToBounds = true
        imageUserIV.layer.cornerRadius = 20
        
        imageUserIV.snp.makeConstraints { (make) in
            
            make.size.equalTo(CGSize(width: 40, height: 40))
            make.top.equalTo(contentView).offset(10)
            make.left.equalToSuperview().offset(20)
            make.bottom.lessThanOrEqualTo(-10)
        }
    }
    
    private func setupNameUser() {
        
        contentView.addSubview(nameUserBt)
        
        nameUserBt.addTarget(self, action: #selector(pressedNameButton), for: .touchUpInside)
        nameUserBt.titleLabel?.font = UIFont.systemFont(ofSize: 14.0, weight: .semibold)
        nameUserBt.setTitleColor(.black, for: .normal)
        nameUserBt.titleLabel?.adjustsFontSizeToFitWidth = true
        nameUserBt.titleLabel?.numberOfLines = 1
        
        nameUserBt.contentHorizontalAlignment = .left
        
        nameUserBt.snp.makeConstraints { (make) in
            
            make.height.equalTo(imageUserIV.snp.height).dividedBy(2)
            make.topMargin.equalTo(imageUserIV.snp.topMargin).offset(2)
            make.left.equalTo(imageUserIV.snp.right).offset(10)
            make.right.equalTo(informationsBt.snp.left).offset(-10)
        }
    }
    
    private func setupLocationUser() {
        
        contentView.addSubview(locationUserBt)
        
        locationUserBt.setImage(UIImage(named: "arrow"), for: .normal)
        locationUserBt.imageView?.contentMode = .scaleAspectFit
        locationUserBt.imageEdgeInsets = UIEdgeInsets(top: 4, left: -6, bottom: 3, right: 10)
        locationUserBt.semanticContentAttribute = UIApplication.shared.userInterfaceLayoutDirection == .rightToLeft ? .forceLeftToRight : .forceRightToLeft
        
        locationUserBt.addTarget(self, action: #selector(pressedLocationButton), for: .touchUpInside)
        locationUserBt.titleLabel?.font = UIFont.systemFont(ofSize: 13.0, weight: .regular)
        locationUserBt.setTitleColor(.black, for: .normal)
        locationUserBt.titleLabel?.adjustsFontSizeToFitWidth = true
        locationUserBt.titleLabel?.numberOfLines = 1
        
        locationUserBt.contentHorizontalAlignment = .left
        
        locationUserBt.snp.makeConstraints { (make) in
            
            make.topMargin.equalTo(imageUserIV.snp.centerYWithinMargins).offset(10)
            make.left.equalTo(imageUserIV.snp.right).offset(10)
            make.right.equalTo(informationsBt.snp.left).offset(-10)
            make.bottomMargin.equalTo(imageUserIV.snp.bottomMargin)
        }
    }
    
    @objc func pressedNameButton() {
        
        print("pressedNameButton")
    }
    
    @objc func pressedLocationButton() {
        
        print("pressedLocationButton")
    }
    
    private func setupInformation() {
        
        contentView.addSubview(informationsBt)
        
        informationsBt.setImage(UIImage(named: "three_dots"), for: .normal)
        
        informationsBt.addTarget(self, action: #selector(pressedInformationButton), for: .touchUpInside)
        
        informationsBt.snp.makeConstraints { (make) in
            
            make.size.equalTo(CGSize(width: 16, height: 16))
            make.right.equalTo(contentView.snp.right).offset(-20)
            make.centerY.equalTo(imageUserIV.snp.centerY)
        }
    }
    
    @objc func pressedInformationButton() {
        
        print("pressedInformationButton")
    }
    
    private func setupPhoto() {
        
        contentView.addSubview(photoIS)
        
        photoIS.circular = false
        photoIS.preload = .all
        photoIS.pageIndicator = nil
        
        photoIS.snp.makeConstraints { (make) in
            
            make.top.equalTo(imageUserIV.snp.bottom).offset(20)
            make.left.equalToSuperview()
            make.right.equalToSuperview()
            make.height.equalTo(contentView.snp.width)
        }
    }
    
    private func setupPageIndicator() {
        
        contentView.addSubview(pageIndicatorPC)
        
        pageIndicatorPC.currentPageIndicatorTintColor = UIColor.systemBlue
        pageIndicatorPC.pageIndicatorTintColor = UIColor.systemGray
        
        pageIndicatorPC.numberOfPages = photoIS.images.count
        
        photoIS.currentPageChanged = ({ [weak self] currentPage in
            
            self?.pageIndicatorPC.currentPage = currentPage
        })
        
        pageIndicatorPC.snp.makeConstraints { (make) in
            
            make.centerX.equalTo(photoIS.snp.centerX)
            make.top.equalTo(photoIS.snp.bottom).offset(16)
        }
    }
    
    private func setupLikeButton() {
        
        contentView.addSubview(likeBt)
        
        likeBt.setImage(UIImage(named: "heart"), for: .normal)
        
        likeBt.addTarget(self, action: #selector(pressedLikeButton), for: .touchUpInside)
        
        likeBt.contentMode = .scaleAspectFill
        
        likeBt.snp.makeConstraints { (make) in
            
            make.size.equalTo(CGSize(width: 24, height: 24))
            make.leadingMargin.equalTo(imageUserIV.snp.leadingMargin)
            make.centerY.equalTo(pageIndicatorPC.snp.centerY)
        }
    }
    
    @objc func pressedLikeButton() {
        
        print("pressedLikeButton")
    }
    
    private func setupCommentButton() {
        
        contentView.addSubview(commentBt)
        
        commentBt.setImage(UIImage(named: "comment"), for: .normal)
        
        commentBt.addTarget(self, action: #selector(pressedCommentButton), for: .touchUpInside)
        
        commentBt.contentMode = .scaleAspectFill
        
        commentBt.snp.makeConstraints { (make) in
            
            make.size.equalTo(likeBt.snp.size)
            make.centerY.equalTo(likeBt.snp.centerY)
            make.leftMargin.equalTo(likeBt.snp.right).offset(20)
        }
    }
    
    @objc func pressedCommentButton() {
        
        print("pressedCommentButton")
    }
    
    private func setupSendButton() {
        
        contentView.addSubview(sendBt)
        
        sendBt.setImage(UIImage(named: "send"), for: .normal)
        
        sendBt.addTarget(self, action: #selector(pressedSendButton), for: .touchUpInside)
        
        sendBt.contentMode = .scaleAspectFill
        
        sendBt.snp.makeConstraints { (make) in
            
            make.size.equalTo(likeBt.snp.size)
            make.centerY.equalTo(likeBt.snp.centerY)
            make.leftMargin.equalTo(commentBt.snp.right).offset(20)
        }
    }
    
    @objc func pressedSendButton() {
        
        print("pressedSendButton")
    }
    
    private func setupFavoriteButton() {
        
        contentView.addSubview(favoriteBt)
        
        favoriteBt.setImage(UIImage(named: "favorite"), for: .normal)
        
        favoriteBt.addTarget(self, action: #selector(pressedFavoriteButton), for: .touchUpInside)
        
        favoriteBt.contentMode = .scaleAspectFill
        
        favoriteBt.snp.makeConstraints { (make) in
            
            make.size.equalTo(likeBt.snp.size)
            make.centerY.equalTo(likeBt.snp.centerY)
            make.rightMargin.equalTo(informationsBt.snp.rightMargin)
        }
    }
    
    @objc func pressedFavoriteButton() {
        
        print("pressedFavoriteButton")
    }
    
    private func setupNumberOfLikesLable() {
        
        contentView.addSubview(numberOfLikesLb)
        
        numberOfLikesLb.numberOfLines = 2
        
        numberOfLikesLb.snp.makeConstraints { (make) in
            
            make.leftMargin.equalTo(likeBt.snp.leftMargin)
            make.rightMargin.equalTo(favoriteBt.snp.rightMargin)
            make.top.equalTo(likeBt.snp.bottom).offset(16)
        }
    }
    
    private func setupCommentsLable() {
        
        contentView.addSubview(commentsLb)
        
        commentsLb.numberOfLines = 0
        
        commentsLb.snp.makeConstraints { (make) in
            
            make.leftMargin.equalTo(numberOfLikesLb.snp.leftMargin)
            make.rightMargin.equalTo(numberOfLikesLb.snp.rightMargin)
            make.top.equalTo(numberOfLikesLb.snp.bottom).offset(10)
        }
    }
    
    private func setupDateLabel() {
        
        contentView.addSubview(dateLb)
        
        dateLb.font = UIFont.systemFont(ofSize: 10)
        dateLb.numberOfLines = 1
        dateLb.textColor = .systemGray
        
        dateLb.snp.makeConstraints { (make) in
            
            make.top.equalTo(commentsLb.snp.bottom).offset(10)
            make.leftMargin.equalTo(commentsLb.snp.leftMargin)
            make.rightMargin.equalTo(commentsLb.snp.rightMargin)
            make.bottom.equalTo(contentView.snp.bottom).offset(-20)
        }
    }
    
    private func setColor(attributedString: NSMutableAttributedString, label: UILabel, string: String, color: UIColor = .black) {
        
        attributedString.setColorForText(textForAttribute: string, withColor: color)
        
        label.attributedText = attributedString
    }
}
