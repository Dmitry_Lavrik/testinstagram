//
//  TestInstagramTests.swift
//  TestInstagramTests
//
//  Created by Dmitry Lavryk on 11.06.2020.
//  Copyright © 2020 Dmitry Lavryk. All rights reserved.
//

import XCTest
@testable import TestInstagram

class TestInstagramTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testNumberOfSectionsIsOne() {
        
        let mainVC = MainViewController()
        let tableView = UITableView()
        
        tableView.dataSource = mainVC
        
        let numberOfSections = tableView.numberOfSections
        
        XCTAssertEqual(numberOfSections, 1)
    }
    
    func testCellHasImageUser() {
        
        let mainVC = MainViewController()
        let tableView = UITableView()
        
        tableView.register(InstagramTableViewCell.self, forCellReuseIdentifier: String(describing: InstagramTableViewCell.self))
        
        tableView.dataSource = mainVC
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: InstagramTableViewCell.self), for: IndexPath(row: 0, section: 0)) as! InstagramTableViewCell
        
        XCTAssertNotNil(cell.imageUserIV)
    }
}

extension TestInstagramTests {
    
    class FakeDataSource: NSObject, UITableViewDataSource {
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return UITableViewCell()
        }
    }
}
